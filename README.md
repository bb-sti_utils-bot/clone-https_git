This repository is used in our synthetic healthchecks to verify that the HTTPS stack is working properly.

For more information about the synthetic checks, please see: https://softwareteams.atlassian.net/browse/BBCDEV-8028
